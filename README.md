# bootmac

Bootmac configures the MAC addresses of WLAN and Bluetooth interfaces at boot.
Bootmac can be invoked in various ways at boot, but currently only udev rules are tested.
Bootmac generates MAC addresses from the `serialno` provided by Android bootloaders
through `/proc/cmdline` or from `/etc/machine-id` with prefix `02:00`.

## License

GPLv3

Copyright (c) Dylan Van Assche (2022)
